 
const int c = 261;
const int d = 294;
const int e = 329;
const int f = 349;
const int g = 391;
const int gS = 415;
const int a = 440;
const int aS = 455;
const int b = 466;
const int cH = 523;
const int cSH = 554;
const int dH = 587;
const int dSH = 622;
const int eH = 659;
const int fH = 698;
const int fSH = 740;
const int gH = 784;
const int gSH = 830;
const int aH = 880;
const int buzzerPin = 5;
const int yled = 2;
const int kled = 3;
int counter = 0;
void setup()
{
//Setup pin modes
pinMode(buzzerPin, OUTPUT);
pinMode(yled, OUTPUT);
pinMode(kled, OUTPUT);
}
void loop()
{
//Play first section
firstSection();
//Play second section

delay(500);
//Repeat second section

}
void beep(int note, int duration)
{
//Play tone on buzzerPin
tone(buzzerPin, note, duration);
//Play different LED depending on value of 'counter'
if(counter % 2 == 0)
{
digitalWrite(yled, HIGH);
delay(duration);
digitalWrite(yled, LOW);
}else
{
digitalWrite(kled, HIGH);
delay(duration);
digitalWrite(kled, LOW);
}
//Stop tone on buzzerPin
noTone(buzzerPin);
delay(50);
//Increment counter
counter++;
}
void firstSection()
{

beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(b, 500);
beep(a, 500);
delay(250);
beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(d, 500);
beep(a, 500);

beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);
delay(250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);

beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(g, 500);
beep(f, 500);
delay(250);
beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(g, 500);
beep(f, 500);

beep(e, 250);
beep(e, 250);
beep(e, 250);
beep(e, 250);
beep(f, 500);
beep(e, 500);
delay(250);
beep(e, 350);
beep(e, 250);
beep(e, 250);
beep(f, 500);
beep(e, 500);
beep(d, 1000);
delay(250);
beep(d, 750);

beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(b, 500);
beep(a, 500);
delay(250);
beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(d, 500);
beep(a, 500);

beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);
delay(250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);

beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(g, 500);
beep(f, 500);
delay(250);
beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(g, 500);
beep(f, 500);

beep(e, 250);
beep(e, 250);
beep(e, 250);
beep(e, 250);
beep(f, 500);
beep(e, 500);
delay(250);
beep(e, 350);
beep(e, 250);
beep(e, 250);
beep(f, 500);
beep(e, 500);
beep(d, 1000);
delay(250);
beep(d, 750);

beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(b, 500);
beep(a, 500);
delay(250);
beep(a, 250);
beep(a, 250);
beep(a, 250);
beep(d, 500);
beep(a, 500);

beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);
delay(250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);

beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(g, 500);
beep(f, 500);
delay(250);
beep(f, 250);
beep(f, 250);
beep(f, 250);
beep(g, 500);
beep(f, 500);

beep(e, 250);
beep(e, 250);
beep(e, 250);
beep(e, 250);
beep(f, 500);
beep(e, 500);
delay(250);
beep(e, 250);
beep(e, 250);
beep(e, 250);
beep(f, 500);
beep(e, 500);
beep(d, 1000);
delay(250);
beep(d, 750);

beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);
delay(250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);
delay(250);

beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);
delay(250);
beep(g, 250);
beep(g, 250);
beep(g, 250);
beep(a, 500);
beep(g, 500);

beep(aH, 4000);
delay(2000);

}

